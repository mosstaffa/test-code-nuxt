import Vue from 'vue'
const plugin = {
  install(Vue) {
    Vue.mixin({
      methods: {
        $fcCustomModal() {
          return {
            show: (id) => {
              this.$root.$emit('showFcCustomModal', id)
            },
            hide: (id) => {
              this.$root.$emit('hideFcCustomModal', id)
            }
          }
        }
      }
    })
  }
}
Vue.use(plugin)
